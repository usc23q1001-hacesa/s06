from abc import ABC, abstractclassmethod

class Person(ABC):
    @abstractclassmethod
    def get_full_name(self):
        pass

    @abstractclassmethod
    def add_request(self):
        pass

    @abstractclassmethod
    def check_request(self):
        pass

    @abstractclassmethod
    def add_user(self):
        pass


class Employee(Person):
    def __init__(self, first_name, last_name, email, department):
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department

    #### setters ####
    def set_first_name(self, first_name):
        self._first_name = first_name

    def set_last_name(self, last_name):
        self._last_name = last_name

    def set_email(self, email):
        self._email = email

    def set_department(self, department):
        self._department = department

    #### getters ####
    def get_first_name(self):
        return self._first_name
    
    def get_last_name(self):
        return self._last_name
    
    def get_email(self):
        return self._email
    
    def get_department(self):
        return self._department
    
    #### Abstract Methods that need to be implemented ####
    def get_full_name(self):
        return f"{self._first_name} {self._last_name}"

    def add_request(self):
        return "Request has been added."
    
    def check_request(self):
        return "Request has been checked."

    def add_user(self):
        return "User has been added."

    def login(self):
        return f"{self._email} has logged in."

    def logout(self):
        return f"{self._email} has logged out."


class TeamLead(Person):
    def __init__(self, first_name, last_name, email, department, members = []):
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department
        self._members = members

    #### setters ####
    def set_first_name(self, first_name):
        self._first_name = first_name

    def set_last_name(self, last_name):
        self._last_name = last_name

    def set_email(self, email):
        self._email = email

    def set_department(self, department):
        self._department = department

    #### getters ####
    def get_first_name(self):
        return self._first_name
    
    def get_last_name(self):
        return self._last_name
    
    def get_email(self):
        return self._email
    
    def get_department(self):
        return self._department
    
    def add_member(self, employee):
        self._members.append(employee)

    def get_members(self):
        return [member.get_full_name() for member in self._members]
        
    #### Abstract Methods that need to be implemented ####
    def get_full_name(self):
        return f"{self._first_name} {self._last_name}"

    def add_request(self):
        return "Request has been added."
    
    def check_request(self):
        return "Request has been checked."

    def add_user(self):
        return "User has been added."

    def login(self):
        return f"{self._email} has logged in."

    def logout(self):
        return f"{self._email} has logged out."


class Admin(Person):
    def __init__(self, first_name, last_name, email, department):
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department

    #### setters ####
    def set_first_name(self, first_name):
        self._first_name = first_name

    def set_last_name(self, last_name):
        self._last_name = last_name

    def set_email(self, email):
        self._email = email

    def set_department(self, department):
        self._department = department

    #### getters ####
    def get_first_name(self):
        return self._first_name
    
    def get_last_name(self):
        return self._last_name
    
    def get_email(self):
        return self._email
    
    def get_department(self):
        return self._department

    #### Abstract Methods that need to be implemented ####
    def get_full_name(self):
        return f"{self._first_name} {self._last_name}"

    def add_request(self):
        return "Request has been added."
    
    def check_request(self):
        return "Request has been checked."

    def add_user(self):
        return "User has been added."

    def login(self):
        return f"{self._email} has logged in."

    def logout(self):
        return f"{self._email} has logged out."


class Request:
    def __init__(self, name, requester, date_requested, status = "pending"):
        self._name = name
        self._requester = requester
        self._date_requested = date_requested
        self._status = status

    # setters
    def set_name(self, name):
        self._name = name
    
    def set_requester(self, requester):
        self._requester = requester

    def set_date_requested(self, date_requested):
        self._date_requested = date_requested

    def set_status(self, status):
        self._status = status

    # getters
    def get_name(self):
        return self._name
    
    def get_requester(self):
        return self._requester

    def get_date_requested(self):
        return self._date_requested
    
    def get_status(self):
        return self._status

    def update_request(self, request):
        self.set_name(request)

    def close_request(self):
        return f"Request '{self._name}' has been closed."

    def cancel_request(self):
        return f"Request '{self._name}' has been cancelled."


employee1 = Employee("John", "Doe", "djohn@gmail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@gmail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@gmail.com", "Marketing")
employee4 = Employee("Brandon", "Smith", "sbrandom@gmail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@gmail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@gmail.com", "Sales")

req1 = Request("New Hire Orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop Repair", employee1, "1-Jul-2021")

#################################################
# For assert, execute `pytest test_activity.py` #
#################################################

teamLead1.add_member(employee3)
teamLead1.add_member(employee4)
for member in teamLead1.get_members():
    print(member)

req2.set_status("closed")
print(req2.close_request())