# execute `pytest test_activity.py`
from activity import employee1, employee2, admin1, teamLead1

def test_1():
    assert employee1.get_full_name() == "John Doe"

def test_2():
    assert admin1.get_full_name() == "Monika Justin"

def test_3():
    assert teamLead1.get_full_name() == "Michael Specter"

def test_4():
    assert employee2.login() == "sjane@gmail.com has logged in."

def test_5():
    assert employee2.add_request() == "Request has been added."

def test_6():
    assert employee2.logout() == "sjane@gmail.com has logged out."

def test_7():
    assert admin1.add_user() == "User has been added."